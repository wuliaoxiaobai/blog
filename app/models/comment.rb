class Comment < ActiveRecord::Base

  belongs_to :post, validate:true
  validates_presence_of :post_id, :body
  validates_presence_of :post
end
